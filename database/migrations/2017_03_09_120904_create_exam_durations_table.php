<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamDurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_durations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('duration');
            $table->boolean('active');
            $table->integer('question_batch_id')->unsigned()->index();
            $table->foreign('question_batch_id')->references('id')->on('question_batches')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_durations');
    }
}
