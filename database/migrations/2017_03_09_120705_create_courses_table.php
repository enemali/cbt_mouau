<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->integer('level_id')->unsigned()->index();
            $table->integer('programme_id')->unsigned()->index();
            $table->integer('department_id')->unsigned()->index();
            $table->integer('department_option_id')->unsigned()->index()->nullable();
            $table->integer('semester_id')->unsigned()->index();
            $table->integer('course_type_id')->unsigned()->index();
            $table->integer('course_mode_id')->unsigned()->index();
            $table->integer('unit');
            $table->boolean('active');
            $table->timestamps();
            $table->foreign('level_id')->references('id')->on('levels')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('programme_id')->references('id')->on('programmes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('departments')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('department_option_id')->references('id')->on('department_options')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('semester_id')->references('id')->on('semesters')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('course_type_id')->references('id')->on('course_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('course_mode_id')->references('id')->on('course_modes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
