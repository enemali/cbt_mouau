<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_answers', function (Blueprint $table) {
            $table->increments('id');  $table->integer('question_serial_number');
            $table->integer('upload_questions_id')->unsigned()->index();
            $table->text('option');
            $table->string('option_choice');
            $table->string('answer_choice');
            $table->boolean('active');
            $table->foreign('upload_questions_id')->references('id')->on('upload_questions')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_answers');
    }
}
