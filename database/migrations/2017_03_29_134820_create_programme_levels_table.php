<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgrammeLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programme_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('programme_id')->unsigned();
            $table->integer('level_id')->unsigned(); 
            $table->foreign('level_id')->references('id')->on('levels');
            $table->foreign('programme_id')->references('id')->on('programmes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programme_levels');
    }
}
