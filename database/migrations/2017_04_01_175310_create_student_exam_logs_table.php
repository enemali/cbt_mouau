<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentExamLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_exam_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_result_id')->unsigned()->index();
            $table->integer('question_id')->unsigned()->index();
            $table->string('answer');
            $table->timestamps();
            $table->foreign('question_id')->references('id')->on('questions')->onUpdate('cascade');
            $table->foreign('student_result_id')->references('id')->on('student_results')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_exam_logs');
    }
}
