<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('serial_number');
            $table->integer('question_header_id')->unsigned()->index();
            $table->text('reference_text');
            $table->string('reference_image_url');
            $table->text('question');
            $table->boolean('is_option_image');
            $table->boolean('is_bonus');
            $table->integer('answer_points');
            $table->timestamps();
            $table->foreign('question_header_id')->references('id')->on('question_headers')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_questions');
    }
}
