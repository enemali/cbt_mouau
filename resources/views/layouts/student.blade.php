<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'CBT') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- jQuery 2.2.3 -->
    <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
    ]); ?>
    </script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>BT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>{{ config('app.name', 'CBT') }}</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="background-color: #3c8dbc">
        <section>
          @if ($student != null )
          <div class="box box-danger" style="background-color: #3c8dbc">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="/dist/img/avatar.png" alt="User profile picture">
              {{-- <h3 class="profile-username text-center"> {{ $student->person->surname}} {{ $student->person->firstname }}</h3> --}}
              <h3 class="profile-username text-center"> {{ session()->get('student')->person->surname }} {{ session()->get('student')->person->firstname }} </h3>
              <p class="text-muted text-center text-black">STUDENT DETAILS </p>
              <ul class="list-group list-group-unbordered" style="background-color: #3c8dbc">
                <li class="list-group-item" style="background-color: #3c8dbc; color: black">
                  <b>Programme</b> <a class="pull-right" style="background-color: #3c8dbc; color: black">{{ session()->get('student')->studentlevel->programme->name }}</a>
                </li>
                <li class="list-group-item" style="background-color: #3c8dbc; color: black">
                  <b>Department</b> <a class="pull-right" style="background-color: #3c8dbc; color: black">{{ session()->get('student')->studentlevel->department->name }}</a>
                </li>
                <li class="list-group-item" style="background-color: #3c8dbc; color: black">
                  <b>Level</b> <a class="pull-right" style="background-color: #3c8dbc; color: black">{{ session()->get('student')->studentlevel->level->name }}</a>
                </li>
              </ul>
              @if (session('examStarted'))
              
              {{-- <span class="info-box-icon text-yellow"><i class="fa fa-clock-o"></i></span>
              <div class="info-box-content text-center">
                <span class="info-box-text"><h3 id="minutes" class="text-yellow" ></h3></span>
                <span class="info-box-text"><p id="seconds" class="text-yellow"></p></span>
              </div> --}}
              <button id="btnEndExam" name="btnEndExam" type="submit" class="btn btn-block btn-danger btn-lg">End Exam</button>
              
              @endif
            </div>
            <!-- /.box-body -->
          </div>
          @endif
        </section>
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.partials.alerts')
        @if (session('examStarted'))
          <div class="col-md-offset-7">
            <div class="info-box-content" style="font-size: 40px">
              <span class="text-black" id="minutes"></span>
              <span class="text-black" id="seconds"></span>
            </div>
          </div>
        @endif
        @yield('content')
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; {{ date('Y')}} <a href="http://lloydant.com">Lloydant Business Services</a>.</strong> All rights
        reserved.
      </footer>
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/plugins/fastclick/fastclick.js"></script>
    <!-- JQUERY CYCle -->
    <script src="/dist/js/jquery.cycle.all.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/app.min.js"></script>
    <script>
    $(function () {
    $("#modelDataTable").DataTable();
    
    });
    </script>
  </body>
</html>