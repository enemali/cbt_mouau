<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p> {{ Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Result Stats</a></li>
          </ul>
        </li>
        @if( Auth::user()->role_id == 1)
          <li class=" treeview">
          <a href="#">
            <i class="fa fa-gear"></i> <span>Exam Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/manager?model=QuestionBatch')}}"><i class="fa fa-circle-o"></i>Manage Question Batch</a></li>
            <li><a href="{{url('/questionHeader')}}"><i class="fa fa-circle-o"></i>Manage Question Header</a></li>
            <li><a href="{{url('/question/all')}}"><i class="fa fa-circle-o"></i>Manage Questions</a></li>
            <li><a href="{{url('/examDuration')}}"><i class="fa fa-circle-o"></i>Manage Exam Duration</a></li>
            <li><a href="{{url('/student/result')}}"><i class="fa fa-circle-o"></i>View Exam Results</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gears"></i> <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/programmeLevel')}}"><i class="fa fa-circle-o"></i>Manage Programme Level</a></li>
            <li><a href="{{url('/programmeDepartment')}}"><i class="fa fa-circle-o"></i>Manage Programme Department</a></li>
            <li><a href="{{url('/manager?model=Faculty')}}"><i class="fa fa-circle-o"></i>Manage Faculty</a></li>
            <li><a href="{{url('/department')}}"><i class="fa fa-circle-o"></i>Manage Department</a></li>
            <li><a href="{{url('/departmentOption')}}"><i class="fa fa-circle-o"></i>Manage Department Option</a></li>
            <li><a href="{{url('/manager?model=Programme')}}"><i class="fa fa-circle-o"></i>Manage Programme</a></li>
            <li><a href="{{url('/manager?model=Level')}}"><i class="fa fa-circle-o"></i>Manage Level</a></li>
            <li><a href="{{url('/manager?model=CourseType')}}"><i class="fa fa-circle-o"></i>Manage Course Type</a></li>
            <li><a href="{{url('/manager?model=CourseMode')}}"><i class="fa fa-circle-o"></i>Manage Course Mode</a></li>
            <li><a href="{{url('/manager?model=Session')}}"><i class="fa fa-circle-o"></i>Manage Session</a></li>
            <li><a href="{{url('/manager?model=Semester')}}"><i class="fa fa-circle-o"></i>Manage Semester</a></li>
            <li><a href="{{url('/manager?model=Sex')}}"><i class="fa fa-circle-o"></i>Manage Sex</a></li>
            <li><a href="{{url('/manager?model=Role')}}"><i class="fa fa-circle-o"></i>Manage Role</a></li>
            <li><a href="{{url('/users')}}"><i class="fa fa-circle-o"></i>Manage Users</a></li>
            <li><a href="{{url('/course')}}"><i class="fa fa-circle-o"></i>Manage Courses</a></li>
            <li><a href="{{url('/student')}}"><i class="fa fa-circle-o"></i>Manage Students</a></li>
          </ul>
        </li>
        @endif
        @if(Auth::user()->role_id == 3)
          <li class="treeview">
            <a href="#">
              <i class="fa fa-gears"></i> <span>Settings</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('/student')}}"><i class="fa fa-circle-o"></i>Manage Students</a></li>
            </ul>
          </li>
         @endif
      </ul>
    </section>