@extends('layouts.app')
@section('content')
<section class="content-header">
  <h1>
  Questions
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
    <li class="active">Create Exam Question</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="container">
    <div class="box">
      <div class="box-header">
        
        <h3 class="box-title">Create Exam Question</h3>
        <a href="{{url('/question/all')}}" class="btn btn-primary pull-right">View All Questions</a>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        
        <!-- form start -->
        <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/savecreatedquestion') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="box box-info">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">

              <div class="form-group">
                <label for="role_id" class="col-sm-2 control-label">Question Header</label>
                <div class="col-sm-10">
                  <select class="form-control" id="batchId" name="headerId" required="required">
                    <option value="" >--Select Question Header--</option>
                    @foreach($QuestionHeaders as $QuestionHeader)
                    <option value="{{$QuestionHeader->id}}">{{$QuestionHeader->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label">QUESTION</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="question" required="required"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label  class="col-sm-2 control-label">a</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="options[]" placeholder="option" required="required">
                </div>
              </div>
              <div class="form-group">
                <label  class="col-sm-2 control-label">b</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="options[]" placeholder="option" required="required">
                </div>
              </div>
              <div class="form-group">
                <label  class="col-sm-2 control-label">c</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="options[]" placeholder="option" required="required">
                </div>
              </div>
              <div class="form-group">
                <label  class="col-sm-2 control-label">d</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="options[]" placeholder="option" required="required">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Correct Answer</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="correctAnswer" placeholder="correct answer" required="required">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Points</label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" name="answerPoints" required="required">
                </div>
              </div>
              <div class="form-group">
              <label class="col-sm-2 control-label">Question Image</label>
                <div class="col-sm-4">
                  <input type="file" class="form-control" name="question_image" >
                </div>
              </div>
            </div>
          </div>
          <div>
            <button id="btnSave" type="submit" class="btn btn-primary">Save</button>
          </div>
          
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function () {
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

});

</script>
@endsection