@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Questions
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Upload Exam Questions</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">

              <h3 class="box-title">Upload Exam Questions</h3>
              <a href="{{url('/question/all')}}" class="btn btn-primary pull-right">View All Questions</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/question') }}" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="box-body">

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Question Header</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="header_id" name="header_id" required="required">
                    <option value="" >--Select Question Header--</option>
                    @foreach($QuestionHeaders as $QuestionHeader)
                    <option value="{{$QuestionHeader->id}}" {{($QuestionHeader->id == $header_id ) ? ' selected="selected"' : ''}}  >{{$QuestionHeader->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>


                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Excel File</label>
                  <div class="col-sm-10">
                   <input type="file" name="import_file" />

                   <a href="/question/sample" > Download Sample Excel </a>
                  </div>
                </div>

                <div>

                </div>

              </div>
               @if ($data == null || $data->count()  <=  0)
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="/question" class="btn btn-default"> Back </a>
                  <button type="submit" class="btn btn-info pull-right">upload Question</button>
                </div>
                <!-- /.box-footer -->
              @endif
            </form>
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/savequestion') }}" enctype="multipart/form-data">
           {{ csrf_field() }}
           <input type="hidden" id="questionHeader" name="questionHeader" value="{{$header_id}}">
            @if ($data != null && $data->count() >  0)
               <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                 @foreach($data[0] as $header => $value)
                  <th>{{$header}}</th>
                @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($data as $datum)
                <tr>
                  @foreach($datum as $question)
                  <td>{{$question}}</td>
                  @endforeach
                </tr>
                @endforeach
              </table>

            <div>
              <button id="btnSave" type="submit" class="btn btn-primary">Save</button>
            </div>

            @endif


           </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

<script type="text/javascript">
  $(document).ready(function () {

     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });



 });


</script>
@endsection
