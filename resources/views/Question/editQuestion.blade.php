@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Questions
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Upload Exam Questions</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
            
              <h3 class="box-title">Upload Exam Questions</h3>
              <a href="{{url('/question/all')}}" class="btn btn-primary pull-right">View All Questions</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
                <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/updatequestion') }}" enctype="multipart/form-data">

           {{ csrf_field() }}

            @if ($allQuestions != null && $allQuestions->count() >  0)
              
                @for($i = 0; $i < $allQuestions->count(); $i++)
                  
                  <input type="hidden" name="header_id[]" value="{{$allQuestions[$i]->question_header_id}}">
                  <input type="hidden" name="serial_number[]" value="{{$allQuestions[$i]->serial_number}}">
                   <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">SN. {{$allQuestions[$i]->serial_number}}</h3>
                          
                          <a href="{{url('/deletequestion/'.$allQuestions[$i]->id)}}" class="btn btn-primary pull-right">Remove</a>
                        </div>
                          <!-- /.box-header -->
                          <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">QUESTION</label>
                              <div class="col-sm-10">
                                <textarea class="form-control" name="question[]">{{$allQuestions[$i]->question}}</textarea>
                              </div>
                            </div>

                             @foreach($allQuestions[$i]->answers as $answer)
                             <div class="form-group">
                                <label  class="col-sm-2 control-label">{{$answer->option}}</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" name="options{{$loop->iteration}}[]" placeholder="option" value="{{$answer->option_choice}}">

                                  @if ($answer->reference_image_url != null)
                                   <div class="form-group">
                                      <label class="col-sm-2 control-label">Option Image</label>
                                      <div class="col-sm-10">
                                       <img src="/question_images/{{$answer->reference_image_url}}" class="img-responsive" height="50px" width="50px">
                                      </div>
                                    </div>
                                  @endif

                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <input type="file" class="form-control" name="option_image_{{$answer->id}}" >
                                    </div>
                                  </div>

                                </div>
                             </div>
                             @endforeach
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Correct Answer</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" name="correct_answer[]" placeholder="correct_answer" value="{{$allQuestions[$i]->answers->first()->answer_choice}}">
                                </div>
                             </div>

                             <div class="form-group">
                                <label class="col-sm-2 control-label">Points</label>
                                <div class="col-sm-10">
                                  <input type="number" class="form-control" name="answer_points[]" value="{{$allQuestions[$i]->answer_points}}">
                                </div>
                             </div>

                             @if ($allQuestions[$i]->reference_image_url != null)
                               <div class="form-group">
                                  <label class="col-sm-2 control-label">Question Image</label>
                                  <div class="col-sm-10">
                                   <img src="/question_images/{{$allQuestions[$i]->reference_image_url}}" class="img-responsive" height="50px" width="50px">
                                  </div>
                                </div>
                                
                             @endif

                              <div class="form-group">
                                <label class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-10">
                                  <input type="file" class="form-control" name="question_image_{{$allQuestions[$i]->serial_number}}" >
                                </div>
                             </div>

                          </div>
                    </div>

                @endfor

            <div>
              <button id="btnSave" type="submit"  class="btn btn-primary">Save</button>
            </div>

            @endif

           
           </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

<script type="text/javascript">
  $(document).ready(function () {
  
     $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

 });
   
</script>
@endsection
