@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
       Exam Duration
        <small>create </small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Create</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/question') }}">
              {{ csrf_field() }}

              <div class="box-body">
                
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Duration (mins)</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" id="duration" name="duration" placeholder="duration" value="{{old('name')}}" >
                  </div>
                </div>
                <input type="hidden" value="1" name="active">

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Question Header</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="batch_id" name="batch_id" required="required">
                    <option value="" >--Select Question Header--</option>
                    @foreach($QuestionHeaders as $QuestionHeader)
                    <option value="{{$QuestionHeader->id}}">{{$QuestionHeader->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/examDuration" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Create Duration</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
