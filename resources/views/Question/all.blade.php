@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Questions
        <small>All uploaded Questions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Questions Uploaded</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Questions</h3>
              <div class="pull-right">
                <a href="{{url('/question')}}" class="btn btn-primary">Upload Questions</a> &nbsp;
                <a href="{{url('/createquestion')}}" class="btn btn-primary">Create Question</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Batch</th>
                  <th>Course</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($allQuestions as $questionHeader)
                <tr>
                   <td>{{$questionHeader->header->name}}</td>
                  <td>{{$questionHeader->header->batch->name}}</td>
                  <td>{{$questionHeader->header->course->code}}</td>
                  <td>{{ ($questionHeader->header->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/question/{{$questionHeader->header->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
