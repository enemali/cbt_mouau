@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Department Options
        <small>All options</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">options</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All options</h3>
              <a href="{{url('/departmentOption/create')}}" class="btn btn-primary pull-right">Create options</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Department</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($Options as $Departments)
                <tr>
                  <td>{{$Departments->name}}</td>
                  <td>{{$Departments->department->name}}</td>
                  <td>{{ ($Departments->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/departmentOption/{{$Departments->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
