@extends('layouts.student')
@section('content')
<section class="content-header">
      <h1>
       {{$allQuestions->first()->header->course->name}}  {{$allQuestions->first()->header->course->code}}
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <form id="frmQuestions" class="form-horizontal" method="POST" action="{{ url('/Exam/end') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if ($allQuestions != null && $allQuestions->count() >  0)
                   <div id="examQuestion" class="box box-info" style="position: relative;">
                    @foreach($allQuestions as $question)
                      
                       <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">SN. {{$loop->iteration}}</h3>
                            </div>

                            <input type="hidden" name="student_id" value="{{session()->get('student')->id}}">
                            <input type="hidden" name="question_header_id" value="{{$question->question_header_id}}">
                            <input type="hidden" name="serial_number[]" value="{{$question->serial_number}}">
                              <!-- /.box-header -->
                              <!-- form start -->
                            <div class="box-body" style="font-size:medium;background-color: rgba(255,255,255,.15);">
                                @if ($question->reference_image_url != null)
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Image</label>
                                  <div class="col-sm-10">
                                      <img src="{{url('/question_images/'.$question->reference_image_url) }}" class="img-responsive" height="100px" width="100px">
                                  </div>
                                </div>
                                @endif

                                <div class="form-group">
                                  <label class="col-sm-2 control-label">QUESTION</label>
                                  <div class="col-sm-10">
                                    <label  name="question[]" >{{$question->question}}</label>
                                  </div>
                                </div>

                                <div class="radio">
                                   @foreach($question->answers as $answer)
                                   <div class="form-group">
                                      <label  class="col-sm-2 control-label">{{$answer->option}}</label>
                                      <div class="col-sm-10">
                                        <input type="radio" name="answer{{$question->serial_number}}"  value="{{$answer->option_choice}}" > {{$answer->option_choice}}
                                        @if($answer->reference_image_url != null)
                                        <img src="{{url('/question_images/'.$answer->reference_image_url) }}" class="img-responsive" height="100px" width="100px">
                                        @endif
                                      </div>
                                   </div>
                                   @endforeach
                                </div>
                               
                              </div>
                        </div>

                    @endforeach
                    </div>
                      <div>
                         <button id="prev" type="button" class="btn btn-danger pull-left">Back <i class="glyphicon glyphicon-bold glyphicon-fast-backward  margin-r-10"></i></button>
                         <button id="next" type="button" class="btn btn-danger pull-right">Next <i class="glyphicon glyphicon-bold glyphicon-fast-forward  margin-r-10"></i></button>
                      </div>
                @endif
               </form>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="row">
            <p>Questions <cite>Attempted Questions appear <span class="label label-success"> green in color </span></cite></p>
             <ul class="fc-color-picker">
              @foreach($allQuestions as $count)
               <span class="label label-primary" id="Ans{{$count->serial_number}}">{{$loop->iteration}}</span> {{-- {{$count->serial_number}} --}}
              @endforeach
            </ul>
          </div>

         <div id="processing" class="col-md-12" style="display:none">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Loading</h3>
            </div>
            <div class="box-body">
              Please wait while the operation completes
            </div>
            <!-- /.box-body -->
            <!-- Loading (remove the following to stop the loading)-->
            <div class="overlay">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
            <!-- end loading -->
          </div>
          <!-- /.box -->
        </div>

      </div>

</section>

<script type="text/javascript">
  $(document).ready(function () {

     $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
     $("#btnEndExam").click(function(){

        var endExam = confirm("Are you sure you want to end this exam ?");
          if (endExam == true) {
              var endExamReally = confirm("Are you really sure ?");
              if (endExamReally == true)
              {
                 $('#frmQuestions').submit();
                 // alert('In your face! Write on jor!');
                  //   $.ajax({
                  //     type: 'POST',
                  //     url: '/Exam/end', // we are calling json method
                  //     dataType: 'json',
                  //     data:$("#frmQuestions").serialize(),
                  //     beforeSend: function () {
                  //           $("#processing").show();
                  //     },
                  //     complete: function () {
                  //         $("#processing").show();
                  //     },
                  // });
              }
          } 
     });
    

      $("#examQuestion").cycle({
            fx: 'fade',
            speed: 'fast',
            timeout: 0,
            next: '#next',
            prev: '#prev'
            // choose your transition type, ex: fade, scrollUp, shuffle, etc...
        });

      $("#next").click(function(){

        var radioBtn = "";
        $('input:radio').each(function() {
          if($(this).is(':checked')) {

            radioBtn = $(this)[0]['name'];
            var labelToColor = "#Ans" + radioBtn.substring(6);
            $(labelToColor).removeClass("label-primary");
            $(labelToColor).addClass("label-success");
          } 
         
        });

      });

      $("#prev").click(function(){

        var radioBtn = "";
        $('input:radio').each(function() {
          if($(this).is(':checked')) {

            radioBtn = $(this)[0]['name'];
            var labelToColor = "#Ans" + radioBtn.substring(6);
            $(labelToColor).removeClass("label-primary");
            $(labelToColor).addClass("label-success");
          } 
         
        });

      });

  // Set the date we're counting down to
    var countDownDate = new Date();
    countDownDate.setMinutes(countDownDate.getMinutes() + parseInt({{$duration}}));
    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();
      
      // Find the distance between now an the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
       stime =  hours + " H " + minutes + " Min " + seconds + " Secs ";
      // $("#time").val(stime); 
       document.getElementById("minutes").innerHTML =  hours + " : " + minutes + " Min";
       document.getElementById("seconds").innerHTML = seconds + " Secs ";
      // If the count down is finished, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("time").innerHTML = "EXPIRED";
        //submit question

      }
    }, 1000);


 });
   

</script>
@endsection
