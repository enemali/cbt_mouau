@extends('layouts.student')
@section('content')

    <!-- Main content -->
    <section class="content">
       <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Exam Center</div>

                <div class="panel-body">
                    <div class="text-center">
                    <p><b>{{ $headerName }}</b></p> <br>
                      <a href="{{url('/Exam/start/'.session()->get('student')->id)}}" class="btn btn-lg btn-primary ">Click here to start Exam</a>
                    </div>

                    <div></div>
                    
                </div>
            </div>
         </div>
         </div>
    </section>

@endsection
