@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
       Programme Level
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">All Programme Level</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Programme Level</h3>
              <a href="{{url('/programmeLevel/create')}}" class="btn btn-primary pull-right">Add Programme Level</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>SN</th>
                  <th>Programme</th>
                  <th>Level</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ProgrammeLevels as $ProgrammeLevel)
                <tr>
                 <td>{{$loop->iteration}}</td> 
                  <td>{{$ProgrammeLevel->programme->name}} minutes</td>
                  <td>{{$ProgrammeLevel->level->name}}</td>
                  <td><a href="/programmeLevel/{{$ProgrammeLevel->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
