@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Course
        <small>create</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Create</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/course') }}">
              {{ csrf_field() }}

              <div class="box-body">

                <input type="hidden" value="1" name="active">
                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Programme</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="" > --Select Programme-- </option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}">{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                    <option value="" >--Select Department--</option>
                    @foreach($Departments as $Department)
                    <option value="{{$Department->id}}">{{$Department->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Option</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_option_id" name="department_option_id">
                    <option value=""> --Select Department Option-- </option>
                    @foreach($DepartmentOptions as $option)
                    <option value="{{$option->id}}">{{$option->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Level</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="level_id" name="level_id" required="required">
                    <option value="" >--Select Level--</option>
                    @foreach($Levels as $Level)
                    <option value="{{$Level->id}}">{{$Level->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Semester</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="semester_id" name="semester_id" required="required">
                    <option value="" >--Select Semester--</option>
                    @foreach($Semesters as $Semester)
                    <option value="{{$Semester->id}}">{{$Semester->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>


               <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Course Mode</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="course_mode_id" name="course_mode_id" required="required">
                    <option value="" >--Select Course Mode--</option>
                    @foreach($CourseModes as $CourseMode)
                    <option value="{{$CourseMode->id}}">{{$CourseMode->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Course Type</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="course_type_id" name="course_type_id" required="required">
                    <option value="" >--Select Course Types--</option>
                    @foreach($CourseTypes as $CourseType)
                    <option value="{{$CourseType->id}}">{{$CourseType->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="name" name="name"  value="{{old('name')}}"></textarea> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Course code</label>
                  <div class="col-sm-10">
                    <input type="text" name="code" id="code" class="form-control" value="{{old('code')}}" /> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Course Unit</label>
                  <div class="col-sm-10">
                    <input type="number" name="unit" id="unit" class="form-control" value="{{old('unit')}}" /> 
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/course" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Create Course</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection


1501597050question-question_image_46-User-me@gmail.com.jpg