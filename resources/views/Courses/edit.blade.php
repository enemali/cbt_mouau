@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Department Options
        <small>edit</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/course').'/'.$course->id }}">
              

              {{ csrf_field() }}
              {{ method_field('PUT') }}

              <div class="box-body">

                <input type="hidden" value="1" name="active">
                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Programme</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="" > --Select Programme-- </option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}" {{($Programme->id == $course->programme_id ) ? ' selected="selected"' : ''}}>{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                    <option value="" >--Select Department--</option>
                    @foreach($Departments as $Department)
                    <option value="{{$Department->id}}" {{($Department->id == $course->department_id ) ? ' selected="selected"' : ''}}>{{$Department->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Option</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_option_id" name="department_option_id" required="required">
                    <option value=""> --Select Department Option-- </option>
                    @foreach($DepartmentOptions as $option)
                    <option value="{{$option->id}}" {{($option->id == $course->department_option_id ) ? ' selected="selected"' : ''}}>{{$option->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Level</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="level_id" name="level_id" required="required">
                    <option value="" >--Select Level--</option>
                    @foreach($Levels as $Level)
                    <option value="{{$Level->id}}" {{($Level->id == $course->level_id ) ? ' selected="selected"' : ''}}>{{$Level->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Semester</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="semester_id" name="semester_id" required="required">
                    <option value="" >--Select Semester--</option>
                    @foreach($Semesters as $Semester)
                    <option value="{{$Semester->id}}" {{($Semester->id == $course->semester_id ) ? ' selected="selected"' : ''}}>{{$Semester->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>


               <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Course Mode</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="course_mode_id" name="course_mode_id" required="required">
                    <option value="" >--Select Course Mode--</option>
                    @foreach($CourseModes as $CourseMode)
                    <option value="{{$CourseMode->id}}" {{($CourseMode->id == $course->course_mode_id ) ? ' selected="selected"' : ''}}>{{$CourseMode->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Course Type</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="course_type_id" name="course_type_id" required="required">
                    <option value="" >--Select Course Types--</option>
                    @foreach($CourseTypes as $CourseType)
                    <option value="{{$CourseType->id}}" {{($CourseType->id == $course->course_type_id ) ? ' selected="selected"' : ''}}>{{$CourseType->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="name" name="name"  > {{ $course->name  }} </textarea> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Course code</label>
                  <div class="col-sm-10">
                    <input type="text" name="code" id="code" class="form-control" value="{{$course->code }}" /> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Course Unit</label>
                  <div class="col-sm-10">
                    <input type="number" name="unit" id="unit" class="form-control" value="{{ $course->unit }}" /> 
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/course" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Update Course</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
