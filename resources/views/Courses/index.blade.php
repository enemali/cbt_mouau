@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Courses
        <small>All Courses</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Courses</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Courses</h3>
              <a href="{{url('/course/create')}}" class="btn btn-primary pull-right">Create Course</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Code</th>
                  <th>Level</th>
                  <th>Programme</th>
                  <th>Department</th>
                  <th>Option</th>
                  <th>Semester</th>
                  <th>Course Type</th>
                  <th>Course Mode</th>
                  <th>Unit</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($Courses as $Course)
                <tr>
                  <td>{{$Course->name}}</td>
                  <td>{{$Course->code}}</td>
                  <td>{{$Course->level->name}}</td>
                  <td>{{$Course->programme->name}}</td>
                  <td>{{$Course->department->name}}</td>
                  <td>{{$Course->option}}</td>
                  <td>{{$Course->semester->name}}</td>
                  <td>{{$Course->courseType->name}}</td>
                  <td>{{$Course->courseMode->name}}</td>
                  <td>{{$Course->unit}}</td>      
                  <td>{{ ($Course->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/course/{{$Course->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
