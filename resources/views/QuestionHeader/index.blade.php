@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Question Headers
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Create Question Header</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All  Question Headers</h3>
              <a href="{{url('/questionHeader/create')}}" class="btn btn-primary pull-right">Create  Question Header</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Batch</th>
                  <th>Course</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($QuestionHeaders as $QuestionHeader)
                <tr>
                  <td>{{$QuestionHeader->name}}</td>
                  <td>{{$QuestionHeader->batch->name}}</td>
                  <td>{{$QuestionHeader->course->code}}</td>
                  <td>{{ ($QuestionHeader->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/questionHeader/{{$QuestionHeader->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
