@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Manage {{ str_plural($modelName) }}
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit {{ $modelName }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/manager/') .'/' .$modelData->id .'?model=' .$modelName }}">
             
              {{ csrf_field() }}
              {{ method_field('PUT') }}

              <div class="box-body">
                
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $modelData->name }}" >
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="active" name="active" required="required">
                    <option value=""  --Select Status-- </option>
                    <option value="1" {{(1 == $modelData->active ) ? ' selected="selected"' : ''}}> Active </option>
                    <option value="0" {{(0 == $modelData->active ) ? ' selected="selected"' : ''}}> Disabled </option>
                  </select>
                  </div>
                </div>


               
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/manager?model={{$modelName}}" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Update {{ $modelName }}</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
