@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Manager <small> All {{ str_plural($modelName) }}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Manager</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>
              <a href="{{url('/manager/create').'?model='.$modelName }}" class="btn btn-primary pull-right">Create {{ $modelName }}</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($modelData as $model)
                <tr>
                  <td>{{$model->name}}</td>
                  <td>{{ ($model->active == 1) ? "Active":"Not Active"}}</td>
                  <td><a href="/manager/{{$model->id}}/edit/?model={{$modelName}} ">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
