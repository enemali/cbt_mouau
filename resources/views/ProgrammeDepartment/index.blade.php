@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
       Programme Department
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">All Programme Department</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Programme Department</h3>
              <a href="{{url('/programmeDepartment/create')}}" class="btn btn-primary pull-right">Add Programme Department</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>SN</th>
                  <th>Programme</th>
                  <th>Department</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ProgrammeDepartments as $ProgrammeDepartment)
                <tr>
                 <td>{{$loop->iteration}}</td> 
                  <td>{{$ProgrammeDepartment->programme->name}}</td>
                  <td>{{$ProgrammeDepartment->department->name}}</td>
                  <td><a href="/programmeDepartment/{{$ProgrammeDepartment->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
