@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Exam Duration
        <small>edit</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/programmeDepartment/'.$ProgrammeDepartment->id) }}">

              {{ csrf_field() }}
              {{ method_field('PUT') }}

              <div class="box-body">


                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Programme</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="">--Select Programme -</option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}" {{($Programme->id == $ProgrammeDepartment->programme_id ) ? ' selected="selected"' : ''}}  >{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <input type="hidden" value="1" name="active">

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                    <option value="">--Select Department--</option>
                    @foreach($Departments as $Department)
                    <option value="{{$Department->id}}" {{($Department->id == $ProgrammeDepartment->department_id ) ? ' selected="selected"' : ''}}  >{{$Department->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/programmeDepartment" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Update Duration</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
