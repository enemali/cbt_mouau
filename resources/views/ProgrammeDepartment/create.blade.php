@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
      Programme Department
        <small>create </small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Create</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/programmeDepartment') }}">
              {{ csrf_field() }}

              <div class="box-body">

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Programme</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="" >--Select Programme -</option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}">{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <input type="hidden" value="1" name="active">

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                    <option value="" >--Select Department--</option>
                    @foreach($Departments as $Department)
                    <option value="{{$Department->id}}">{{$Department->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/programmeDepartment" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Add Programme Department</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
