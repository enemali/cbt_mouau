@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Exam Duration
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Create Exam Duration</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All   Exam Duration</h3>
              <a href="{{url('/examDuration/create')}}" class="btn btn-primary pull-right">Create   Exam Duration</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Exam Date</th>
                  <th>Duration</th>
                  <th>Batch</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($ExamDuration as $duration)
                <tr>
                 <td>{{$duration->exam_date}}</td> 
                  <td>{{$duration->duration}} minutes</td>
                  <td>{{$duration->batch->name}}</td>
                  <td>{{ ($duration->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/examDuration/{{$duration->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
