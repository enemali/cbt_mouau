@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Exam Duration
        <small>edit</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/examDuration/'.$examDuration->id) }}">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="box-body">

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Exam Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="exam_date" name="exam_date" placeholder="exam_date" value="{{$examDuration->exam_date}}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Exam Duration</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="duration" name="duration" placeholder="duration" value="{{$examDuration->duration}}">
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Question Batch</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="batch_id" name="question_batch_id" required="required">
                    <option value="" >--Select Question Batch--</option>
                    @foreach($QuestionBatches as $QuestionBatche)
                    <option value="{{$QuestionBatche->id}}" {{($QuestionBatche->id == $examDuration->question_batch_id ) ? ' selected="selected"' : ''}}  >{{$QuestionBatche->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="active" name="active" required="required">
                    <option value="" >--Select Status--</option>
                    <option value="1" {{(1 == $examDuration->active ) ? ' selected="selected"' : ''}} >Active</option>
                    <option value="0" {{(0 == $examDuration->active ) ? ' selected="selected"' : ''}} >Disabled</option>
                  </select>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/examDuration" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Update Duration</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
