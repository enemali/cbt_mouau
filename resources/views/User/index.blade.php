@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Users
        <small>All users</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Users</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Users</h3>
              <a href="{{url('/users/create')}}" class="btn btn-primary pull-right">Create User</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Account Name</th>
                  <th>Account Email</th>
                  <th>Status</th>
                  <th>Last login</th>
                  <th></th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($Users as $user)
                <tr>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{ ($user->active == 1) ? "Yes":"No"}}</td>
                  <td>{{$user->last_login}}</td>
                  <td><a href="/users/edit/{{$user->id}}">Edit</a></td>
                  <td><a href="/users/reset/{{$user->id}}">Reset Password</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
