@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Student Results
        <small>All Results</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Results</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Matric Number</th>
                  <th>Sex</th>
                  <th>Level</th>
                  <th>Programme</th>
                  <th>Department</th>
                  <th>Course Title</th>
                  <th>Course Code</th>
                  <th>Semester</th> 
                  <th>Score</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                <tr>
                  <td>{{$result->student->person->surname}} {{$result->student->person->firstname}} {{$result->student->person->othername}}</td>
                  <td>{{$result->student->matric_number}}</td>
                  <td>{{$result->student->person->sex->name}}</td>
                  <td>{{$result->student->studentLevel->level->name}}</td>
                  <td>{{$result->student->studentLevel->programme->name}}</td>
                  <td>{{$result->student->studentLevel->department->name}}</td>
                  <td>{{$result->questionHeader->course->name}}</td>
                  <td>{{$result->questionHeader->course->code}}</td>
                  <td>{{$result->questionHeader->course->semester->name}}</td> 
                  <td>{{$result->score}}</td>
                </tr>
                @endforeach
              </table>
              <div class="form-group">
                <button href="#" class="btn btn-primary">Send Sms</button>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
