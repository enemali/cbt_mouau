@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Student
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Upload Student</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">

              <h3 class="box-title">Upload Student</h3>
              <a href="{{url('/student/')}}" class="btn btn-primary pull-right">View All Students</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            @if ($data == null || $data->count()  <=  0)
                <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/student/create') }}" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="box-body">

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Session </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="session_id" name="session_id" required="required">
                    <option value="" >--Select Session --</option>
                    @foreach($Sessions as $Session)
                    <option value="{{$Session->id}}" >{{$Session->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="programme_id" class="col-sm-2 control-label">Programme </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="" >--Select Programme --</option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}" >{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>

                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Level </label>
                  <div class="col-sm-10">
                    <select class="form-control" id="level_id" name="level_id" required="required">
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Excel File</label>
                  <div class="col-sm-10">
                   <input type="file" name="import_file" required="required" />
                   <a href="/student/sample" > Download Sample Excel </a>
                  </div>
                </div>

                <div>

                </div>

              </div>

                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="/student" class="btn btn-default"> Back </a>
                  <button type="submit" class="btn btn-info pull-right">upload Students</button>
                </div>
                <!-- /.box-footer -->

            </form>
            @endif
            <form id="frmCreate" class="form-horizontal" method="POST">
           {{ csrf_field() }}

                 <table id="modelDataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>SN</th>
                      <th>MATRIC NUMBER</th>
                      <th>SURNAME</th>
                      <th>FIRSTNAME</th>
                      <th>OTHERNAME</th>
                      <th>SEX</th>
                    </tr>
                    </thead>
                     @if ($data != null && $data->count() >  0)
                      <tbody>
                      @foreach($data as $datum)
                      <tr>
                        @foreach($datum as $question)
                        <td>{{$question}}</td>
                        @endforeach
                      </tr>
                      @endforeach
                      </tbody>
                     @endif
                 </table>
           </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

 <script type="text/javascript">
   $(document).ready(function () {

      $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

        $("#programme_id").change(function () {
          // alert("Changed!");
          var programme_id = $("#programme_id").val();

          $.ajax({
                type: 'POST',
                url: '/getDepartmentByProgramme', // we are calling json method
                dataType: 'json',
                data: { id: programme_id },
                success: function (Departments) {
                  // console.log(Users);
                    $("#department_id").empty();
                    $("#department_id").append('<option value=""> -- Select Department -- </option>');

                    $.each(Departments, function (i, Department) {
                         for(var j = 0; j < Department.length; j++)
                         {
                           $("#department_id").append('<option value="' + Department[j].id + '">' + Department[j].name + '</option>');
                         }

                    });
                },
                error: function (ex) {
                    console.log(ex);
                    alert('Failed to retrieve Departments.' + ex);
                }
            });

           $.ajax({
                type: 'POST',
                url: '/getLevelByProgramme', // we are calling json method
                dataType: 'json',
                data: { id: programme_id },
                success: function (Levels) {
                  // console.log(Users);
                    $("#level_id").empty();
                    $("#level_id").append('<option value=""> -- Select Level -- </option>');

                    $.each(Levels, function (i, Level) {
                         for(var j = 0; j < Level.length; j++)
                         {
                           $("#level_id").append('<option value="' + Level[j].id + '">' + Level[j].name + '</option>');
                         }

                    });
                },
                error: function (ex) {
                    console.log(ex);
                    alert('Failed to retrieve Departments.' + ex);
                }
            });

        });
   });

  </script>
@endsection
