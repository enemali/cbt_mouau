@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Students
        <small>All Students</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Students</h3>
              <a href="{{url('/student/create')}}" class="btn btn-primary pull-right">Create Student</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Matric Number</th>
                  <th>Sex</th>
                  <th>Level</th>
                  <th>Programme</th>
                  <th>Department</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                <tr>
                  <td>{{$student->person->surname}} {{$student->person->firstname}} {{$student->person->othername}}</td>
                  <td>{{$student->matric_number}}</td>
                  <td>{{$student->person->sex->name}}</td>
                  <td>{{$student->studentLevel->level->name}}</td>
                  <td>{{$student->studentLevel->programme->name}}</td>
                  <td>{{$student->studentLevel->department->name}}</td>
                  <td>{{ ($student->active == 1) ? "Active":"Not Active"}}</td>
                  <td><a href="/student/{{$student->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
