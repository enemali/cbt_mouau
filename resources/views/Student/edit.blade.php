@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Student Detail
        <small>edit</small>
      </h1>
      
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
          <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="frmCreate" class="form-horizontal" method="POST" action="{{ url('/student').'/'.$student->id .'/edit'}}">
              

              {{ csrf_field() }}

              <div class="box-body">


                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Surname</label>
                  <div class="col-sm-10">
                    <input type="text" name="surname" id="surname" class="form-control" value="{{$student->person->surname }}" /> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Firstname</label>
                  <div class="col-sm-10">
                    <input type="text" name="firstname" id="firstname" class="form-control" value="{{$student->person->firstname }}" /> 
                  </div>
                </div>

                 <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Othername</label>
                  <div class="col-sm-10">
                    <input type="text" name="othername" id="othername" class="form-control" value="{{ $student->person->othername }}" /> 
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Sex</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="sex_id" name="sex_id" required="required">
                    <option value="" > --Select Sex-- </option>
                    @foreach($Sexes as $sex)
                    <option value="{{$sex->id}}" {{($sex->id == $student->person->sex_id ) ? ' selected="selected"' : ''}}>{{$sex->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Programme</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="programme_id" name="programme_id" required="required">
                    <option value="" > --Select Programme-- </option>
                    @foreach($Programmes as $Programme)
                    <option value="{{$Programme->id}}" {{($Programme->id == $student->studentLevel->programme_id ) ? ' selected="selected"' : ''}}>{{$Programme->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Department</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_id" name="department_id" required="required">
                    <option value="" >--Select Department--</option>
                    @foreach($Departments as $Department)
                    <option value="{{$Department->id}}" {{($Department->id == $student->studentLevel->department_id ) ? ' selected="selected"' : ''}}>{{$Department->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 @if ($student->studentLevel->department_option_id != null )
                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Option</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="department_option_id" name="department_option_id" required="required">
                    <option value=""> --Select Department Option-- </option>
                    @foreach($DepartmentOptions as $option)
                    <option value="{{$option->id}}" {{($option->id == $student->studentLevel->department_option_id ) ? ' selected="selected"' : ''}}>{{$option->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>
                @endif

                <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Level</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="level_id" name="level_id" required="required">
                    <option value="" >--Select Level--</option>
                    @foreach($Levels as $Level)
                    <option value="{{$Level->id}}" {{($Level->id == $student->studentLevel->level_id ) ? ' selected="selected"' : ''}}>{{$Level->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                 <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Session</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="session_id" name="session_id" required="required">
                    <option value="" >--Select Session--</option>
                    @foreach($Sessions as $Session)
                    <option value="{{$Session->id}}" {{($Session->id == $student->studentLevel->session_id ) ? ' selected="selected"' : ''}}>{{$Session->name}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>


                   <div class="form-group">
                  <label for="role_id" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="active" name="active" required="required">
                    <option value=""  --Select Status-- </option>
                    <option value="1" {{(1 == $student->active ) ? ' selected="selected"' : ''}}> Active </option>
                    <option value="0" {{(0 == $student->active ) ? ' selected="selected"' : ''}}> Disabled </option>
                  </select>
                  </div>
                </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/student" class="btn btn-default"> Back </a>
                <button type="submit" class="btn btn-info pull-right">Update student record</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
         
        </div>
      </div>
   </section>

@endsection
