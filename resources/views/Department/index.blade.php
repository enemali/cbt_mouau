@extends('layouts.app')
@section('content')
<section class="content-header">
      <h1>
        Departments
        <small>All Departments</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Settings</a></li>
        <li class="active">Department</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Departments</h3>
              <a href="{{url('/department/create')}}" class="btn btn-primary pull-right">Create Department</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="modelDataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Faculty</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($Departments as $Departments)
                <tr>
                  <td>{{$Departments->name}}</td>
                  <td>{{$Departments->faculty->name}}</td>
                  <td>{{ ($Departments->active == 1) ? "Yes":"No"}}</td>
                  <td><a href="/department/{{$Departments->id}}/edit/">Edit</a></td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
</section>

@endsection
