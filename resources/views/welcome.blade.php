<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Michael Okpara University</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

        <!-- jQuery 2.2.3 -->
        <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
              background-image: url('/dist/img/bg2.jpg');
              background-repeat:no-repeat;
              background-position: center; 
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                background-color: black;
                opacity: 0.5;
            }

            .m-b-md {
                margin-bottom: 30px;
                color: black;
                display: block;
                background-color: white;
                opacity: 0.5;
            }

        </style>
    </head>
    <body>
                <div >
                    <img src="{{ asset('images/MOUAU.jpg') }}">
                </div>
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/admin/login') }}">Admin Login</a>
                    @endif
                </div>
        <div class="flex-center position-ref full-height">
           
            <div class="content">
                <div class="title m-b-md">
                     <a href="{{ url('/login') }}">Click Here to Login</a>
                </div>

                <div class="links">
                    <a href="#" data-toggle="modal" data-target="#howtouse">How to Use</a>
                    <a href="#" data-toggle="modal" data-target="#about">About Lloydant</a>
                    <a href="#" data-toggle="modal" data-target="#support">Support Desk</a>
                </div>
            </div>
        </div>
       
<div id="howtouse" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">How to Use</h4>
      </div>
      <div class="modal-body">
          <div id="testmodal" style="padding: 5px 20px;">
           FlashCBT is the easiest computer based examination solution
          </div>
        </div>
    </div>
    </div>
</div>

<div id="about" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">About Lloydant</h4>
      </div>
      <div class="modal-body">
          <div id="testmodal" style="padding: 5px 20px;">
           At LloydAnt we help our customers do business better by leveraging our industry-wide experience, deep technology expertise and comprehensive service portfolio to vertically align their business models and deliver excellent business capabilities. We use our industry insight, technological vision and innovative thinking to produce fresh perspectives on our customers’ businesses ensuring that they accelerate their business and ultimately reach full potential.
          </div>
        </div>
    </div>
    </div>
</div>

<div id="support" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Support Desk</h4>
      </div>
      <div class="modal-body">
          <div id="testmodal" style="padding: 5px 20px;">
           You can reach our support desk via the following channels: <br/>
           <span><i class="glyphicon glyphicon-phone"></i>+2347088391544</span><br/>
           <span><i class="glyphicon glyphicon-phone"></i>+2348164958768</span><br/>
           <span><i class="glyphicon glyphicon-envelope"></i>support@lloydant.com</span><br/>

          </div>
        </div>
    </div>
    </div>
</div>

    </body>
</html>
