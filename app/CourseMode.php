<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseMode extends Model
{
   //
     protected $fillable = [
        'name', 'active', 
    ];
}
