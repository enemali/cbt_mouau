<?php

namespace App;

use App\Department;
use App\DepartmentOption;
use App\Level;
use App\Programme;
use App\Session;
use Illuminate\Database\Eloquent\Model;

class StudentLevel extends Model
{
    //
      protected $fillable = [
        'department_id','programme_id','department_option_id','level_id','code','session_id',
    ];

     public function department()
    {
    	return $this->belongsTo(Department::class);
    }

     public function programme()
    {
    	return $this->belongsTo(Programme::class);
    }

     public function option()
    {
    	return $this->belongsTo(DepartmentOption::class);
    }

     public function level()
    {
    	return $this->belongsTo(Level::class);
    }

     public function session()
    {
    	return $this->belongsTo(Session::class);
    }


}
