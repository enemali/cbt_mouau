<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    public function header()
    {
    	return $this->belongsTo(QuestionHeader::class);
    }
     public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    public function batch()
    {
    	return $this->belongsTo(QuestionBatch::class);
    }

    public function answers()
    {
    	return $this->hasMany(QuestionAnswer::class);
    }
}
