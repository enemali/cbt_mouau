<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeDepartment extends Model
{
   //
      protected $fillable = [
        'department_id','programme_id',
    ];

     public function department()
    {
    	return $this->belongsTo(Department::class);
    }
     public function programme()
    {
    	return $this->belongsTo(Programme::class);
    }
}
