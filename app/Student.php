<?php

namespace App;

use App\Person;
use App\StudentLevel;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
     protected $fillable = [
        'person_id', 'matric_number','password','active'
    ];

    public function person()
    {
    	return $this->belongsTo(Person::class);
    }

    public function studentLevel()
    {
    	return $this->hasOne(StudentLevel::class);
    }

    
}
