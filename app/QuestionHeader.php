<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionHeader extends Model
{
    //
     protected $fillable = [
        'name', 'active','question_batch_id','course_id',
    ];

    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    public function batch()
    {
    	return $this->belongsTo(QuestionBatch::class);
    }
}
