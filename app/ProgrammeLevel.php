<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgrammeLevel extends Model
{
      protected $fillable = [
        'level_id','programme_id',
    ];

     public function level()
    {
    	return $this->belongsTo(Level::class);
    }
     public function programme()
    {
    	return $this->belongsTo(Programme::class);
    }
}
