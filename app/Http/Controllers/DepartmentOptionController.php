<?php

namespace App\Http\Controllers;

use App\DepartmentOption;
use App\Department;
use App\Faculty;
use Illuminate\Http\Request;

class DepartmentOptionController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Options = DepartmentOption::all();
        return view('DepartmentOption.index')->with('Options',$Options);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $Departments = Department::whereActive(1)->get();
        return view('DepartmentOption.create')->with('Departments',$Departments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DepartmentOption::create($request->all());
        return redirect('/departmentOption')->with('status','Option was added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentOption  $departmentOption
     * @return \Illuminate\Http\Response
     */
    // public function show(DepartmentOption $departmentOption)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentOption  $departmentOption
     * @return \Illuminate\Http\Response
     */
    public function edit(DepartmentOption $departmentOption)
    {
        //
        $Departments = Department::whereActive(1)->get();
        return view('DepartmentOption.edit')->with('Departments',$Departments)->with('Option',$departmentOption);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentOption  $departmentOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentOption $departmentOption)
    {
        //
        $departmentOption->update($request->all());
        return redirect('/departmentOption')->with('info','Option was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentOption  $departmentOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentOption $departmentOption)
    {
        //
    }
}
