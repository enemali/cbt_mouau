<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    

    public function register()
    {
        return view('auth.register');
    }

    public function registerPost(Request $request)
    {
         $this->validate($request, [
           'name' => 'required|max:255',
           'email' => 'required|email|max:255|unique:users',
           'password' => 'required|min:6|confirmed',
        ]);


        $user =  new User;
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->active = 1;
        $user->password = bcrypt($request['password']);
        $user->last_login = date('Y-m-d');
        $user->save();


        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']]))
        {
            return redirect('/home');
        }
        return view('auth.register');

    }
}
