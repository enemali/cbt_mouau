<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Student;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginPost(Request $request)
    {
        // return $request->all();

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password'],'active'=>'1'], $request['remember'] ))
        {
           
            $user = Auth::user();
            $user->last_login = date('Y-m-d H:i:s');
            $user->ip_address = $request->ip();
            $user->save();

            return redirect('/home');
        }

        return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

     public function Studentlogin()
    {
        return view('auth.studentLogin');
    }

    public function StudentloginPost(Request $request)
    {
        // return $request->all();
        $matric_number = $request['matric_number'];
        $password = $request['password'];
        $student = Student::where('matric_number',$matric_number)->get();
        // return $student;
        if ($student != null && $student->count() > 0)
        {
            if ($password == $student->first()->matric_number)
            {
                session(['student' => $student->first()]);
                return redirect('/studenthome');
            }
        }
        return view('auth.studentLogin');
    }

}
