<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseMode;
use App\CourseType;
use App\ProgrammeDepartment;
use App\DepartmentOption;
use App\Faculty;
use App\ProgrammeLevel;
use App\Programme;
use App\Role;
use App\Semester;
use App\Session;
use App\Sex;
use App\Student;
use Illuminate\Http\Request;

class UtilityController extends Controller
{
	public function getDepartmentByProgramme(Request $request)
    {   
        $Departments = array();
        $programmeDepartments = ProgrammeDepartment::where('programme_id',$request['id'])->get();
        for($i = 0; $i < count($programmeDepartments); $i++)
        {
           
            $Departments[$i] = $programmeDepartments[$i]->department;
           
            
        }
        return response()->json(array('Departments'=> $Departments), 200); 
    }

    public function getLevelByProgramme(Request $request)
    {
        $Levels = array();
        $programmeLevels = ProgrammeLevel::where('programme_id',$request['id'])->get();
        for($i = 0; $i < count($programmeLevels); $i++)
        {
            $Levels[$i] = $programmeLevels[$i]->level;
        }
        return response()->json(array('Levels'=> $Levels), 200); 

    }
}