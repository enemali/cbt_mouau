<?php

namespace App\Http\Controllers;

use App\ExamDuration;
use App\QuestionHeader;
use App\QuestionBatch;
use App\course;
use Illuminate\Http\Request;

class ExamDurationController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ExamDurations = ExamDuration::all();
        if ($ExamDurations != null && count($ExamDurations) > 0) {
            foreach ($ExamDurations as $ExamDuration) {
                $ExamDuration->batch = QuestionBatch::find($ExamDuration->question_batch_id);
            }
        }
        return view('ExamDuration.index')->with('ExamDuration',$ExamDurations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $QuestionBatches = QuestionBatch::whereActive(1)->get();
        return view('ExamDuration.create')->with('QuestionBatches',$QuestionBatches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if batch already has duration
       $status = "Exam Duration already set for the selected batch";
       $duration = ExamDuration::where('question_batch_id',$request['question_batch_id'])->get()->first();
       if ($duration == null || $duration = "")
       { 
           ExamDuration::create($request->all());
           $status ='Exam Duration was added';
           return redirect('/examDuration')->with('status',$status);
       }

        return redirect('/examDuration')->with('warning',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function show(Department $department)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamDuration $examDuration)
    {
        //
        $QuestionBatches = QuestionBatch::whereActive(1)->get();
        return view('ExamDuration.edit')->with('QuestionBatches',$QuestionBatches)->with('examDuration',$examDuration);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamDuration $examDuration)
    {
        //
        $examDuration->update($request->all());
        return redirect('/examDuration')->with('info','Exam Duration was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Department $department)
    // {
    //     //
    // }
}
