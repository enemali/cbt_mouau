<?php

namespace App\Http\Controllers;

use App\Course;
use App\Faculty;
use App\Programme;
use App\Level;
use App\CourseType;
use App\Semester;
use App\Session;
use App\Sex;
use App\CourseMode;
use App\Role;
use App\DepartmentOption;
use App\Department;
use Illuminate\Http\Request;

class CourseController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        $Courses = Course::all();
        return view('Courses.index')->with('Courses',$Courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Departments = Department::whereActive(1)->get();
        $Programmes = Programme::whereActive(1)->get();
        $Levels = Level::whereActive(1)->get();
        $CourseTypes = CourseType::whereActive(1)->get();
        $Semesters = Semester::whereActive(1)->get();
        $CourseModes = CourseMode::whereActive(1)->get();
        $DepartmentOptions = DepartmentOption::whereActive(1)->get();
        return view('Courses.create')->with('Departments',$Departments)->with('Programmes',$Programmes)->with('Levels',$Levels)->with('CourseTypes',$CourseTypes)->with('Semesters',$Semesters)->with('CourseModes',$CourseModes)->with('DepartmentOptions',$DepartmentOptions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Course::create($request->all());
        return redirect('/course')->with('status','course was added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
        $Departments = Department::whereActive(1)->get();
        $Programmes = Programme::whereActive(1)->get();
        $Levels = Level::whereActive(1)->get();
        $CourseTypes = CourseType::whereActive(1)->get();
        $Semesters = Semester::whereActive(1)->get();
        $CourseModes = CourseMode::whereActive(1)->get();
        $DepartmentOptions = DepartmentOption::whereActive(1)->get();
        return view('Courses.edit')->with('Departments',$Departments)->with('Programmes',$Programmes)->with('Levels',$Levels)->with('CourseTypes',$CourseTypes)->with('Semesters',$Semesters)->with('CourseModes',$CourseModes)->with('DepartmentOptions',$DepartmentOptions)->with('course',$course);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
        $course->update($request->all());
        return redirect('/course')->with('info','course was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
