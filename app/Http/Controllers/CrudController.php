<?php 

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Faculty;
use App\Programme;
use App\Level;
use App\CourseType;
use App\Semester;
use App\Session;
use App\Sex;
use App\CourseMode;
use App\Role;
use App\QuestionBatch;
use Illuminate\Support\Facades\Input;

class CrudController extends Controller
{
    const MODEL_KEY = 'model';
    var $modelData;
    var $modelName;
    var $model;

    protected $modelsMapping = [
        'Faculty' => Faculty::class,
        'Programme' => Programme::class,
        'Level' => Level::class,
        'CourseType' => CourseType::class,
        'Semester' => Semester::class,
        'Session' => Session::class,
        'Sex' => Sex::class,
        'CourseMode' => CourseMode::class,
        'Role' => Role::class,
        'QuestionBatch' => QuestionBatch::class
    ];

    protected function getModel() {
        $modelKey = Input::get(static::MODEL_KEY);
        if (array_key_exists($modelKey, $this->modelsMapping)) {
            return $this->modelsMapping[$modelKey];
        }

        throw new \InvalidArgumentException('Invalid model');
    }

     public function __construct()
    {
        $this->middleware('auth');
        $this->model = $this->getModel();
        $this->modelName = explode("\\",$this->model)[1]; 
    }

    public function index()
    {
        $this->modelData = $this->model::all();
        return view('Crud.index')->with('modelData', $this->modelData)->with('modelName',$this->modelName);
    }

    public function create()
    {
        return view('Crud.create')->with('modelName',$this->modelName);
    }
    public function store()
    {
        $this->model::create(array_except(Input::all(), static::MODEL_KEY));
        return redirect('/manager?model='.$this->modelName)->with('status',$this->modelName. ' was created!');
    }

    // public function show($id)
    // {
    //     $model = $this->getModel();
    //     return $model::findOrFail($id);
    // }

    public function edit($id)
    {
        $this->modelData = $this->model::findOrFail($id);
        return view('Crud.edit')->with('modelData', $this->modelData)->with('modelName',$this->modelName);
    }

    public function update($id)
    {
        $object = $this->model::findOrFail($id);
        $object->update(array_except(Input::all(), static::MODEL_KEY));
        return redirect('/manager?model='.$this->modelName)->with('info',$this->modelName. ' was updated!');
    }

    // public function destroy($id)
    // {
    //     $model = $this->getModel();
    //     return $model::remove($id);
    // }
    
}