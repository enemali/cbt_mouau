<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseMode;
use App\CourseType;
use App\Department;
use App\DepartmentOption;
use App\Faculty;
use App\Level;
use App\Person;
use App\Programme;
use App\Role;
use App\Semester;
use App\Session;
use App\Sex;
use App\Student;
use App\StudentLevel;
use App\StudentResult;
use App\Utility;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use \DB;

class StudentController extends Controller
{
	public function index()
	{
		$students = Student::all();
		return view('Student.index')->with('students',$students);
	}

	 public function createStudent()
    {
        //
        $data = [];
        $Programmes = Programme::all();
        $Levels = Level::all();
        $Sessions = Session::all();
        $Departments = Department::all();
        $DepartmentOptions = DepartmentOption::all();
        return view('Student.create')->with('Programmes',$Programmes)->with('Levels',$Levels)->with('Sessions',$Sessions)->with('Departments',$Departments)->with('DepartmentOptions',$DepartmentOptions)->with('data',$data);
    }

    public function createStudentPost(Request $request)
    {
    	  try {

		        $Programmes = Programme::all();
		        $Levels = Level::all();
		        $Sessions = Session::all();
		        $Departments = Department::all();
		        $DepartmentOptions = DepartmentOption::all();
                $data = [];

                if(Input::hasFile('import_file') && $request['department_id'] != null ){

                    if ($request['programme_id'] != null && $request['department_id'] != null && $request['level_id'] != null && $request['session_id'] != null)
                    {
                    	$programme = Programme::find($request['programme_id']);
                    	$level = Level::find($request['level_id']);
                    	$session = Session::find($request['session_id']);
                    	$department = Department::find($request['department_id']);

                        $path = Input::file('import_file')->getRealPath();
                        $data = Utility::uploadExcel($path);
                        $excelFile = $request->file('import_file');
                        $input['import_file'] = time().'studentUpload-'.'-User-'.Auth::user()->email. '.' .$excelFile->getClientOriginalExtension();
                        $destinationPath = public_path('/question_excel');
                        $excelFile->move($destinationPath, $input['import_file']);
                        $this->saveDataTemporarily($data,$programme,$level,$session,$department);
                        return view('Student.create')->with('Programmes',$Programmes)->with('Levels',$Levels)->with('Sessions',$Sessions)->with('Departments',$Departments)->with('DepartmentOptions',$DepartmentOptions)->with('data',$data);

                    }
                }
                return back()->with('warning','File upload failed please try again');
        } catch (Exception $e) {

        }
    }

    public function saveDataTemporarily($data ,Programme $programme,Level $level,Session $session,Department $department )
    {
        DB::transaction(function () use($data, $programme,$level,$session,$department) {

                //Get data
                foreach ($data as $datum)
                {
                	$person = new Person;
                	$person->surname = $datum["surname"];
                	$person->firstname = $datum["firstname"];
                	$person->othername = $datum["othername"];
                	$person->passport_url = "-";
                	$person->sex_id = ($datum["sex"] == "M" ) ? '1' : '2';
                	$person->save();

                    $student = new Student;
                    $student->person_id = $person->id;
                    $student->matric_number = $datum["matric_number"];
                    $student->password = bcrypt($datum["matric_number"]) ;
                    $student->active = 1;
                    $student->save();

                    $studentLevel = new StudentLevel;
                    $studentLevel->student_id = $student->id;
                    $studentLevel->level_id = $level->id;
                    $studentLevel->programme_id = $programme->id ;
                    $studentLevel->department_id = $department->id;
                    $studentLevel->session_id = $session->id;
                    $studentLevel->save();

                }
        });
    }

    public function editStudent(Student $student)
    {
    	$Programmes = Programme::all();
        $Levels = Level::all();
        $Sessions = Session::all();
        $Departments = Department::all();
        $DepartmentOptions = DepartmentOption::all();
        $Sexes = Sex::all();
    	return view('Student.edit')->with('Programmes',$Programmes)->with('Levels',$Levels)->with('Sessions',$Sessions)->with('Departments',$Departments)->with('DepartmentOptions',$DepartmentOptions)->with('Sexes',$Sexes)->with('student',$student);
    }

    public function editStudentPost(Request $request,Student $student)
    {
    	$student->update($request->all());
    	$student->person->update($request->all());
    	$student->studentLevel->update($request->all());
    	return redirect('/student')->with('status','student was updated successfully');
    }

    public function viewResults()
    {
        $results = StudentResult::all();
        return view('Student.result')->with('results',$results);
    }

    public function downloadSampleExcel()
    {
        try {

            $data = array(
            array("SN"=>"1", "MATRIC_NUMBER"=>"MOUA/2017/00001", "SURNAME"=>"ANEKE", "FIRSTNAME"=>"EMEKA", "OTHERNAME"=>"JOHN", "SEX"=>"M")
            );

            Utility::exportData('studentUploadSample',$data,'xlsx');

        } catch (Exception $e) {

        }
    }
}