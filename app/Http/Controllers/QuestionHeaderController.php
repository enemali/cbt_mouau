<?php

namespace App\Http\Controllers;

use App\QuestionHeader;
use App\QuestionBatch;
use App\Course;
use Illuminate\Http\Request;

class QuestionHeaderController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $QuestionHeaders = QuestionHeader::all();
        if ($QuestionHeaders != null && count($QuestionHeaders) > 0) {
            foreach ($QuestionHeaders as $questionHeader) {
                $questionHeader->batch = QuestionBatch::find($questionHeader->question_batch_id);
            }
        }
        return view('QuestionHeader.index')->with('QuestionHeaders',$QuestionHeaders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $QuestionBatches = QuestionBatch::whereActive(1)->get();
        $courses = Course::whereActive(1)->get();
        return view('QuestionHeader.create')->with('QuestionBatches',$QuestionBatches)->with('courses',$courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = "Question Header already exists for the selected course and batch";
        //check if Question header exists for the course and batch selected.
        $oldHeader = QuestionHeader::where('question_batch_id',$request['question_batch_id'])->where('course_id',$request['course_id'])->get()->first();
        if ($oldHeader == null || $oldHeader == "")
        {
            QuestionHeader::create($request->all());
            $status ='Question Header was added';
            return redirect('/questionHeader')->with('status', $status);
        }
        return redirect('/questionHeader')->with('warning', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function show(Department $department)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionHeader $questionHeader)
    {
        //
        $QuestionBatches = QuestionBatch::whereActive(1)->get();
        $courses = Course::whereActive(1)->get();
        return view('QuestionHeader.edit')->with('QuestionBatches',$QuestionBatches)->with('courses',$courses)->with('questionHeader',$questionHeader);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionHeader $questionHeader)
    {
        //
        $questionHeader->update($request->all());
        return redirect('/questionHeader')->with('info','Question Header was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Department $department)
    // {
    //     //
    // }
}
