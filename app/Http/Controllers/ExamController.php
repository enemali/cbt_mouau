<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseMode;
use App\CourseType;
use App\DepartmentOption;
use App\Faculty;
use App\Programme;
use App\ProgrammeDepartment;
use App\ProgrammeLevel;
use App\Question;
use App\QuestionHeader;
use App\Role;
use App\Semester;
use App\Session;
use App\Sex;
use App\Student;
use App\StudentResult;
use Illuminate\Http\Request;
use \DB;

class ExamController extends Controller
{

	public function __construct()
	{

	}

	public function index()
    {
        if (session('student') == null)
        {
            return redirect('/login');
        }
        $student = null;
        $student = session('student');
        session(['student' => $student->first()]);

        $questionDetail = DB::select("SELECT question_headers.id, question_headers.name AS headerName, exam_durations.exam_date,exam_durations.duration,question_headers.id,question_headers.course_id,question_batches.name FROM exam_durations, question_headers, question_batches,courses WHERE exam_durations.question_batch_id = question_headers.question_batch_id AND question_batches.id = exam_durations.question_batch_id AND question_headers.course_id = courses.id AND question_headers.active = 1 AND courses.level_id=? AND courses.programme_id = ? AND courses.department_id=?  AND exam_durations.active = 1",[$student->first()->studentLevel->level_id,$student->first()->studentLevel->programme_id,$student->first()->studentLevel->department_id]);
        
        if ($questionDetail == null || count($questionDetail) <= 0) {
            return redirect('/login')->with('status',' You do not have any exam scheduled today or the exam settings was not completed by administrator.');
        }

        return view('ExamCenter.index')->with('student', $student)->with('headerName', $questionDetail[0]->headerName);
  //   	if (session('student') == null)
		// {
		// 	return redirect('/login');
		// }
  //       $id = session('student')->id;
  //       $student = Student::where('id',$id)->get();
  //       session(['student' => $student[0]]);
  //       $myStudent = $student[0];
		// return view('ExamCenter.index')->with('student', $myStudent);
    }

    public function startExam(Student $student)
    {
    	if (session('student') == null)
		{
			return redirect('/login');
		}
		session(['examStarted' => true]);
    	$questionDetail = DB::select("SELECT question_headers.id,exam_durations.exam_date,exam_durations.duration,question_headers.id,question_headers.course_id,question_batches.name FROM exam_durations, question_headers, question_batches,courses WHERE exam_durations.question_batch_id = question_headers.question_batch_id AND question_batches.id = exam_durations.question_batch_id AND question_headers.course_id = courses.id AND question_headers.active = 1 AND courses.level_id=? AND courses.programme_id = ? AND courses.department_id=?  AND exam_durations.active = 1",[$student->studentLevel->level_id,$student->studentLevel->programme_id,$student->studentLevel->department_id]);
        
        if ($questionDetail != null && count($questionDetail) >0)
        {
            $allQuestions = Question::where('question_header_id',$questionDetail[0]->id)->get()->shuffle();
            if ($allQuestions != null && count($allQuestions) > 0) {
                foreach ($allQuestions as $question) {
                    $question->header = QuestionHeader::find($question->question_header_id);
                    if ($question->header != null) {
                        $question->header->course = Course::find($question->header->course_id);
                    }
                }
            }
            $studentResult = StudentResult::where('student_id', $student->id)->get();
            if ($studentResult != null && count($studentResult) > 0 && $studentResult[0]->question_header_id == $questionDetail[0]->id ) 
            {
                return redirect('/login')->with('status','You have taken this exam!');
            }
            return view('ExamCenter.exam')->with('allQuestions',$allQuestions)->with('duration',$questionDetail[0]->duration);
        }
		return redirect('/login')->with('status','Exam settings not completed by administrator');
    }
    public function endExam(Request $request)
    {
    	$correct = [];
    	$score = 0;
    	
    	$questions = Question::where('question_header_id',$request['question_header_id'])->get();
    	foreach ($questions as $question) 
    	{
    		$answer = "answer";
    		$answer = $answer.$question->serial_number ;
    		if ($request[$answer] && $request[$answer] == $question->answers->first()->answer_choice)
    		{
				$score = $score+1;
				$correct[] = $answer;
    		}
    	}

    	$result = new StudentResult;
    	$result->student_id = $request['student_id'];
    	$result->question_header_id = $request['question_header_id'];
    	$result->score = $score;
    	$result->save();
    	$request->session()->flush();
    	return redirect('/login')->with('status','Thank you for writing! Your results will be published shortly');
    }
}