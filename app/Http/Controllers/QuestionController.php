<?php

namespace App\Http\Controllers;

use App\ExamDuration;
use App\Question;
use App\QuestionAnswer;
use App\QuestionBatch;
use App\QuestionHeader;
use App\UploadAnswers;
use App\UploadQuestions;
use App\Utility;
use App\Course;
use Auth;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use \DB;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $data = [];
        $QuestionHeaders = QuestionHeader::all();
        return view('Question.index')->with('QuestionHeaders',$QuestionHeaders)->with('header_id','')->with('data',$data);
    }


    public function uploadQuestion(Request $request)
    {
        try {
                $QuestionHeaders = QuestionHeader::all();
                $data = [];

                if(Input::hasFile('import_file') && $request['header_id'] != null ){

                    $questionHeader = QuestionHeader::find($request['header_id']);
                    if ($questionHeader != null && count($questionHeader) > 0)
                    {
                        $path = Input::file('import_file')->getRealPath();
                        $data = Utility::uploadExcel($path);
                        $excelFile = $request->file('import_file');
                        $input['import_file'] = time().'header-'.$questionHeader->id.'-User-'.Auth::user()->email. '.' .$excelFile->getClientOriginalExtension();
                        $destinationPath = public_path('/question_excel');
                        $excelFile->move($destinationPath, $input['import_file']);
                        $this->saveDataTemporarily($data,$questionHeader);
                        return view('Question.index')->with('data',$data)->with('QuestionHeaders',$QuestionHeaders)->with('header_id',$request['header_id']);

                    }
                }
                return back()->with('warning','File upload failed please try again');
        } catch (Exception $e) {

        }
    }

    public function saveDataTemporarily($data ,QuestionHeader  $questionHeader )
    {
        DB::transaction(function () use($data, $questionHeader) {

               $UploadQuestionsList = [];
               $UploadAnswers = [];
                //Get data
                foreach ($data as $datum)
                {
                    $question = new UploadQuestions;
                    $question->serial_number = $datum["sn"];
                    $question->question = $datum["question"];
                    $correct_answer = $datum["answer"];
                    $question->answer_points = $datum["points"];
                    $question->question_header_id = $questionHeader->id;
                    $question->is_bonus = false;
                    $question->is_option_image = false;
                    $question->save();
                    $option = "a";

                    for($i = 0; $i < $datum->count() - 4; $i++)
                    {
                        if (isset($datum[$option]))
                        {
                            $answer = new UploadAnswers;
                            $answer->question_serial_number = $question->serial_number;
                            $answer->upload_questions_id = $question->id;
                            $answer->option = $option;
                            $answer->option_choice = $datum[$option];
                            $answer->answer_choice = $correct_answer;
                            $answer->active = true;
                            $answer->save();
                            // var_dump($answer);
                            ++$option;

                        }

                    }

                }
        });
    }

    public function saveQuestion(Request $request)
    {
        try {

               DB::transaction(function () use($request) {

                $questionHeader = QuestionHeader::find($request['questionHeader']);
                if ($questionHeader != null)
                {
                    //Check if date of exam has passed
                   $duration = ExamDuration::where('question_batch_id',$questionHeader->question_batch_id)->whereDate('exam_date','<=',date('Y-m-d'))->get();
                   if ($duration != null)
                   {
                        //Check if exam has been written
                       $uploadedQuestions = UploadQuestions::where('question_header_id',$questionHeader->id)->get();
                       foreach ($uploadedQuestions as $questionUpload)
                       {

                            $question = new Question;
                            $question->serial_number = $questionUpload->serial_number;
                            $question->question = $questionUpload->question;
                            $question->answer_points = $questionUpload->answer_points;
                            $question->question_header_id = $questionUpload->question_header_id;
                            $question->is_bonus = $questionUpload->is_bonus;
                            $question->is_option_image = $questionUpload->is_option_image;
                            $question->save();

                            foreach ($questionUpload->answers as $option) {

                                $answer = new QuestionAnswer;
                                $answer->question_serial_number = $option->question_serial_number;
                                $answer->question_id = $question->id;
                                $answer->option = $option->option;
                                $answer->option_choice = $option->option_choice;
                                $answer->answer_choice = $option->answer_choice;
                                $answer->active = $option->active;
                                $answer->save();
                            }

                       }
                   }

                       ExamDuration::where('question_batch_id',$questionHeader->question_batch_id)->whereDate('exam_date','<=',date('Y-m-d'))->delete();


                }
            });
            return back()->with('status','Question has saved!');

        } catch (Exception $e) {

             return back()->with('warning','An error occured! Please try again');
        }
    }
    public function AllQuestions()
    {
        $allQuestions = Question::all()->unique('question_header_id');
        if ($allQuestions != null && count($allQuestions) > 0) {
            foreach ($allQuestions as $allQuestion) {
                $allQuestion->header = QuestionHeader::find($allQuestion->question_header_id);
                if ($allQuestion->header != null) {
                    $allQuestion->header->batch = QuestionBatch::find($allQuestion->header->question_batch_id);
                    $allQuestion->header->course = Course::find($allQuestion->header->course_id);
                }
            }
        }
        return view('Question.all')->with('allQuestions',$allQuestions);

    }
    public function editQuestion($id)
    {
        $allQuestions =Question::where('question_header_id',$id)->get();
        return view('Question.editQuestion')->with('allQuestions',$allQuestions);
    }
    public function updateQuestion(Request $request)
    {
        // return $request->question_image_45->getClientOriginalExtension();
         // DB::transaction(function () use($request) {

                // return $request->all();
                $headers = $request['header_id'];
                $question = $request['question'];
                $options1 = $request['options1'];
                $options2 = $request['options2'];
                $options3 = $request['options3'];
                $options4 = $request['options4'];
                $options5 = $request['options5'];
                $answers = $request['correct_answer'];
                $points = $request['answer_points'];
                $bonus = $request['is_bonus'];
                $serials = $request['serial_number'];

                $id = $headers[0];
                $allQuestions = Question::where('question_header_id',$id)->get();
                if (count($headers) == count($allQuestions))
                {
                    foreach ($allQuestions as $key=>$value)
                    {
                        //dd($serials[$key]);
                        $image = "question_image_".$serials[$key];
                        $value->question = $question[$key];
                        $value->answer_points = $points[$key];

                        $answercount = $value->answers->count();
                        if ($value->answers[0])
                        {
                            $optionImage = "option_image_".$value->answers[0]->id;
                            if ($request[$optionImage] != null)
                            {
                                $path = Input::file($optionImage)->getRealPath();
                                $imageFile = $request[$optionImage];
                                $input[$optionImage] = time().'question-'.$optionImage.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                                $destinationPath = public_path('/question_images');
                                $imageFile->move($destinationPath, $input[$optionImage]);
                                $value->answers[0]->reference_image_url = $input[$optionImage];
                            }
                            $value->answers[0]->option_choice = $options1[$key];
                            $value->answers[0]->answer_choice = $answers[$key];
                            $value->answers[0]->save();
                        }
                        if ($value->answers[1])
                        {
                            $optionImage = "option_image_".$value->answers[1]->id;
                            if ($request[$optionImage] != null)
                            {
                                $path = Input::file($optionImage)->getRealPath();
                                $imageFile = $request[$optionImage];
                                $input[$optionImage] = time().'question-'.$optionImage.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                                $destinationPath = public_path('/question_images');
                                $imageFile->move($destinationPath, $input[$optionImage]);
                                $value->answers[1]->reference_image_url = $input[$optionImage];
                            }
                            $value->answers[1]->option_choice = $options2[$key];
                            $value->answers[1]->answer_choice = $answers[$key];
                            $value->answers[1]->save();
                        }
                        if ($value->answers[2])
                        {
                            $optionImage = "option_image_".$value->answers[2]->id;
                            if ($request[$optionImage] != null)
                            {
                                $path = Input::file($optionImage)->getRealPath();
                                $imageFile = $request[$optionImage];
                                $input[$optionImage] = time().'question-'.$optionImage.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                                $destinationPath = public_path('/question_images');
                                $imageFile->move($destinationPath, $input[$optionImage]);
                                $value->answers[2]->reference_image_url = $input[$optionImage];
                            }
                            $value->answers[2]->option_choice = $options3[$key];
                            $value->answers[2]->answer_choice = $answers[$key];
                            $value->answers[2]->save();
                        }
                        if ($value->answers[3])
                        {
                            $optionImage = "option_image_".$value->answers[3]->id;
                            if ($request[$optionImage] != null)
                            {
                                $path = Input::file($optionImage)->getRealPath();
                                $imageFile = $request[$optionImage];
                                $input[$optionImage] = time().'question-'.$optionImage.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                                $destinationPath = public_path('/question_images');
                                $imageFile->move($destinationPath, $input[$optionImage]);
                                $value->answers[3]->reference_image_url = $input[$optionImage];
                            }
                            $value->answers[3]->option_choice = $options4[$key];
                            $value->answers[3]->answer_choice = $answers[$key];
                            $value->answers[3]->save();
                        }

                        if ($request->$image != null)
                        {
                            //dd($request->$image);
                            //  $value->reference_image_url
                            $path = Input::file($image)->getRealPath();
                            $imageFile = $request->file($image);
                            $input[$image] = time().'question-'.$image.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                            $destinationPath = public_path('/question_images');
                            $imageFile->move($destinationPath, $input[$image]);
                            $value->reference_image_url = $input[$image];
                        }
                        $value->save();
                    }
                }



         // });

        return redirect('/question/all')->with('status','changes made to question');
    }
    public function createQuestion()
    {
        $questionHeaders = QuestionHeader::All();
        return view('Question.createQuestion')->with('QuestionHeaders',$questionHeaders);
    }
    public function saveCreatedQuestion(Request $request)
    {
        $options = $request['options'];
        $questionHeader = QuestionHeader::where('id',$request['headerId'])->get()->last();
         //dd($questionHeader->first());
        if ($questionHeader != null) {
            $existingQuestion = Question::where('Question_header_id', $questionHeader->id)->get()->last();
           // dd($existingQuestion);
            $questionSerialnumber = 1;
            if ($existingQuestion != null) {
                $questionSerialnumber = $existingQuestion->serial_number + 1;
            }

        DB::transaction(function () use($request, $questionSerialnumber, $options, $questionHeader) {
            $question = new Question;
            $question->serial_number = $questionSerialnumber;
            $question->question = $request['question'];
            $question->answer_points = $request['answerPoints'];
            $question->question_header_id = $questionHeader->id;
            $question->is_bonus = 0;
            $question->is_option_image = 0;

            $questionImage = "question_image";
            if ($request[$questionImage] != null)
            {
                $path = Input::file($questionImage)->getRealPath();
                $imageFile = $request[$questionImage];
                $input[$questionImage] = time().'question-'.$questionImage.'-User-'.Auth::user()->email. '.' .$imageFile->getClientOriginalExtension();
                $destinationPath = public_path('/question_images');
                $imageFile->move($destinationPath, $input[$questionImage]);
                $question->reference_image_url = $input[$questionImage];
            }

            $question->save();

            foreach ($options as $key=>$value) {

                $answer = new QuestionAnswer;
                $answer->question_serial_number = $questionSerialnumber;
                $answer->question_id = $question->id;
                switch ($key) {
                    case 0:
                        $answer->option = 'a';
                        break;
                    case 1:
                        $answer->option = 'b';
                        break;
                    case 2:
                        $answer->option = 'c';
                        break;
                    case 3:
                        $answer->option = 'd';
                        break;
                    default:
                        $answer->option = 'a';
                        break;
                }

                $answer->option_choice = $value;
                $answer->answer_choice = $request['correctAnswer'];
                $answer->active = 1;
                $answer->save();
            }
        });
        return redirect('/createquestion')->with('status','Question has been added.');
        }
    }
    public function deleteQuestion($id)
    {
        if ($id != null) {
          $deleted = Question::find($id)->delete();
          if ($deleted) {
              return redirect('/question/all')->with('status','Question has been removed.');
          }else {
              return redirect('/question/all')->with('warning','Question was not removed.');
          }
        }
    }

     public function downloadSampleExcel()
    {
        try {

            $data = array(
            array("SN"=>"1","REFERENCE TEXT" => "" ,"QUESTION"=>"The Lectures are always captivating ", "A"=>"BORING", "B"=>"NOT INTERESTING", "C"=>"INTERESTING", "D"=>"CAPTIVATING", "ANSWER" => "BORING" ,"POINTS" => "2")
            );

            Utility::exportData('questionUploadSample',$data,'xlsx');

        } catch (Exception $e) {

        }
    }
}