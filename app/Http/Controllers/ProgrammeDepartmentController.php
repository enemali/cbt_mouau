<?php

namespace App\Http\Controllers;


use App\ProgrammeDepartment;
use App\Department;
use App\Programme;
use Illuminate\Http\Request;

class ProgrammeDepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ProgrammeDepartments = ProgrammeDepartment::all();
        return view('ProgrammeDepartment.index')->with('ProgrammeDepartments',$ProgrammeDepartments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Programmes = Programme::whereActive(1)->get();
        $Departments = Department::whereActive(1)->get();
        return view('ProgrammeDepartment.create')->with('Programmes',$Programmes)->with('Departments',$Departments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if batch already has duration
       $status = "Programme Department already set for the selected department and programme";
       $ProgrammeDepartment = ProgrammeDepartment::where('department_id',$request['department_id'])->where('programme_id',$request['programme_id'])->get()->first();
       if ($ProgrammeDepartment == null || $ProgrammeDepartment = "")
       { 
           ProgrammeDepartment::create($request->all());
           $status ='Programme Department was added';
           return redirect('/programmeDepartment')->with('status',$status);
       }

        return redirect('/programmeDepartment')->with('warning',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function show(Department $department)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgrammeDepartment $programmeDepartment)
    {
        //
        $Programmes = Programme::whereActive(1)->get();
        $Departments = Department::whereActive(1)->get();
        return view('ProgrammeDepartment.edit')->with('Programmes',$Programmes)->with('Departments',$Departments)->with('ProgrammeDepartment',$programmeDepartment);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgrammeDepartment $programmeDepartment)
    {
        //
        $status = "Programme Department already set for the selected department and programme";
       
        $ProgrammeDepartment = ProgrammeDepartment::where('department_id',$request['department_id'])->where('programme_id',$request['programme_id'])->get()->first();
        if ($ProgrammeDepartment == null || $ProgrammeDepartment = "")
        { 
            $programmeDepartment->update($request->all());
            return redirect('/programmeDepartment')->with('info','Programme Department was updated');
        }
        return back()->with('warning',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Department $department)
    // {
    //     //
    // }
}
