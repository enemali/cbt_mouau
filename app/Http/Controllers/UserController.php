<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
class UserController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Get all users
        $Users = User::all();
        return view('User.index')->with('Users',$Users);
    }

    public function create()
    {
        //Create User Page
        $Roles = Role::all();
        return view('User.create')->with('Roles',$Roles);
    }

    public function createUser(Request $request)
    {

        try {
              // Validate User creation parameters
             $this->validate($request, [
               'name' => 'required|max:255',
               'email' => 'required|email|max:255|unique:users',
               'password' => 'required|min:6',

            ]);

            $user =  new User;
            $user->name = $request['name'];
            $user->email = $request['email'];
            $user->active = 1;
            $user->password = bcrypt($request['password']);
            $user->role_id = $request['role_id'];
            $user->last_login = date('Y-m-d');
            $user->save();

            return redirect('/users')->with('status','User was created!');
            
        } catch (Exception $e) {

            return back()->with('warning',$e);
        }
    }

    
    public function edit(User $id)
    {
        //
        $Roles = Role::all();
        return view('User.edit')->with('User',$id)->with('Roles',$Roles);
    }

   
    public function update(Request $request,User $id)
    {
         // Validate User creation parameters
             $this->validate($request, [
               'name' => 'required|max:255',
               'email' => 'required|email|max:255',

            ]);

             $id->name = $request['name'];
             $id->active = $request['active'];
             $id->role_id = $request['role_id'];
             $id->save();

            return redirect('/users')->with('info','User was '. $id->email .' updated!');

    }

    public function reset(User $id)
    {
        //
        $id->password = bcrypt("password");
        $id->save();
        return back()->with('info','The account ' . $id->email . ' has been reset');
    }
}
