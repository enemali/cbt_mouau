<?php

namespace App\Http\Controllers;


use App\Department;
use App\Level;
use App\Programme;
use App\ProgrammeLevel;
use Illuminate\Http\Request;

class ProgrammeLevelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ProgrammeLevels = ProgrammeLevel::all();
        return view('ProgrammeLevel.index')->with('ProgrammeLevels',$ProgrammeLevels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $Programmes = Programme::whereActive(1)->get();
        $Levels = Level::whereActive(1)->get();
        return view('ProgrammeLevel.create')->with('Programmes',$Programmes)->with('Levels',$Levels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if batch already has duration
       $status = "Programme Level already set for the selected level and programme";
       $ProgrammeDepartment = ProgrammeLevel::where('level_id',$request['level_id'])->where('programme_id',$request['programme_id'])->get()->first();
       if ($ProgrammeDepartment == null || $ProgrammeDepartment = "")
       { 
           ProgrammeLevel::create($request->all());
           $status ='Programme Level was added';
           return redirect('/programmeLevel')->with('status',$status);
       }

        return redirect('/programmeLevel')->with('warning',$status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function show(Department $department)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgrammeLevel $programmeLevel)
    {
        //
        $Programmes = Programme::whereActive(1)->get();
        $Levels = Level::whereActive(1)->get();
        return view('ProgrammeLevel.edit')->with('Programmes',$Programmes)->with('Levels',$Levels)->with('programmeLevel',$programmeLevel);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgrammeDepartment $programmeDepartment)
    {
        //
        $status = "Programme Department already set for the selected department and programme";
       
        $ProgrammeDepartment = ProgrammeDepartment::where('level_id',$request['level_id'])->where('programme_id',$request['programme_id'])->get()->first();
        if ($ProgrammeDepartment == null || $ProgrammeDepartment = "")
        { 
            $programmeDepartment->update($request->all());
            return redirect('/programmeLevel')->with('info','Programme Department was updated');
        }
        return back()->with('warning',$status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Department $department)
    // {
    //     //
    // }
}
