<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepartmentOption extends Model
{
     //
     protected $fillable = [
        'name', 'active','department_id', 
    ];

     public function department()
    {
    	return $this->belongsTo(Department::class);
    }
}
