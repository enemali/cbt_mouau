<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamDuration extends Model
{
    //
    //
     protected $fillable = [
        'duration', 'active','question_batch_id','exam_date'
    ];

    public function batch()
    {
    	return $this->belongsTo(QuestionBatch::class);
    }
}
