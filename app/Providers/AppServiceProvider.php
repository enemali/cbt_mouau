<?php

namespace App\Providers;

use App\Student;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('layouts.student', function($view){

            if (session('student') != null)
            {
                $student = session('student');
                $student = Student::find($student->first()->id);
                $view->with('student',$student->first());
            }
            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
