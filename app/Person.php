<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    //
      protected $fillable = [
        'surname','firstname','othername','passport_url','sex_id',
    ];
    public function sex()
    {
    	return $this->belongsTo(Sex::class);
    }
}
