<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentResult extends Model
{
    //
    public function student()
    {
    	return $this->belongsTo(Student::class);
    }

     public function questionHeader()
    {
    	return $this->belongsTo(QuestionHeader::class);
    }
}
