<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadQuestions extends Model
{
    //
     //
     protected $fillable = [
        'name', 'active','question_batch_id','course_id',
    ];

    public function header()
    {
        return $this->belongsTo(QuestionHeader::class);
    }
    
    public function course()
    {
    	return $this->belongsTo(Course::class);
    }

    public function batch()
    {
    	return $this->belongsTo(QuestionBatch::class);
    }

    public function answers()
    {
    	return $this->hasMany(UploadAnswers::class);
    }
}
