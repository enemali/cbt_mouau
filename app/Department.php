<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
     protected $fillable = [
        'name', 'active','faculty_id', 
    ];


    public function faculty()
    {
    	return $this->belongsTo(Faculty::class);
    }

    public function options()
    {
    	return $this->hasMany(DepartmentOption::class);
    }
}
