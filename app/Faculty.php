<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    //
     protected $fillable = [
        'name', 'active', 
    ];

    public function faculty()
    {
    	return $this->hasMany(Department::class);
    }
}
