<?php

namespace App;

use Auth;
use App\Question;
use App\QuestionHeader;
use App\QuestionBatch;
use App\course;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Input;

class Utility 
{
	//$data is the array to export and $type can be pdf,xls,xlsx,csv
	public static function exportData($filename,$data,$format)
	{
		try {

			return Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('Sheet1', function($sheet) use ($data)
                {
                   $sheet->fromArray($data);
                      });
                         })->download($format);
			
		} catch (Exception $e) {

			
		}
                    
	}

	//$path is the full path location of the file
	public static function uploadExcel($path)
	{
		try {
			   $data = Excel::load($path, function($reader){})->get();
			   return $data;
		} catch (Exception $e) {
			// throw new Exception('Error Occured while trying to export File', code = 0, previous = null);
		}
	}

}