<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //
      protected $fillable = [
        'name', 'active','department_id','programme_id','department_option_id','level_id','code','semester_id','course_type_id','course_mode_id','unit',
    ];

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }
     public function programme()
    {
    	return $this->belongsTo(Programme::class);
    }
     public function courseMode()
    {
    	return $this->belongsTo(CourseMode::class);
    }
     public function courseType()
    {
    	return $this->belongsTo(courseType::class);
    }
     public function option()
    {
    	return $this->belongsTo(DepartmentOption::class);
    }
     public function faculty()
    {
    	return $this->belongsTo(Faculty::class);
    }
     public function level()
    {
    	return $this->belongsTo(Level::class);
    }
     public function semester()
    {
    	return $this->belongsTo(Semester::class);
    }

}
