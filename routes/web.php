<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Registration & Account Routes

Route::get('/admin/login', 'Auth\LoginController@login')->name('login');
Route::post('/admin/login', 'Auth\LoginController@loginPost')->name('login');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@register')->name('register');
Route::post('/register', 'Auth\RegisterController@registerPost')->name('register');

Route::get('/home', 'HomeController@index');

//Users
Route::get('/users', 'UserController@index')->name('users');
Route::get('/users/create', 'UserController@create');
Route::post('/users/create', 'UserController@createUser');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::post('/users/edit/{id}', 'UserController@update');
Route::get('/users/reset/{id}', 'UserController@reset');

//CRUD
Route::resource('manager', 'CrudController');
Route::resource('department', 'DepartmentController');
Route::resource('departmentOption', 'DepartmentOptionController');
Route::resource('course', 'CourseController');
Route::resource('questionHeader', 'QuestionHeaderController');
Route::resource('examDuration', 'ExamDurationController');
Route::resource('programmeDepartment', 'ProgrammeDepartmentController');
Route::resource('programmeLevel', 'ProgrammeLevelController');

//Questions
Route::get('/question', 'QuestionController@index');
Route::post('/question', 'QuestionController@uploadQuestion');
Route::post('/savequestion', 'QuestionController@saveQuestion');
Route::get('/question/sample', 'QuestionController@downloadSampleExcel');
Route::get('/question/all', 'QuestionController@AllQuestions');
Route::get('/question/{id}/edit', 'QuestionController@editQuestion');
Route::post('/updatequestion', 'QuestionController@updateQuestion');
Route::get('/createquestion', 'QuestionController@createQuestion');
Route::post('/savecreatedquestion', 'QuestionController@saveCreatedQuestion');
Route::get('/deletequestion/{id}', 'QuestionController@deleteQuestion');

//Questions
Route::post('/getDepartmentByProgramme', 'UtilityController@getDepartmentByProgramme');
Route::post('/getLevelByProgramme', 'UtilityController@getLevelByProgramme');
Route::post('/getLevelByProgrammes', 'UtilityController@getDepartmentByProgramme');


//Students
Route::get('/student', 'StudentController@index');
Route::get('/student/create', 'StudentController@createStudent');
Route::post('/student/create', 'StudentController@createStudentPost');
Route::get('/student/sample', 'StudentController@downloadSampleExcel');
Route::get('/student/{student}/edit', 'StudentController@editStudent');
Route::post('/student/{student}/edit', 'StudentController@editStudentPost');
Route::get('/student/result', 'StudentController@viewResults');


// Exam Login & routes
Route::get('/login', 'Auth\LoginController@Studentlogin')->name('Studentlogin');
Route::post('/login', 'Auth\LoginController@StudentloginPost')->name('Studentlogin');

Route::get('/studenthome', 'ExamController@index');
Route::get('/Exam/start/{student}', 'ExamController@startExam');
Route::post('/Exam/end', 'ExamController@endExam');







